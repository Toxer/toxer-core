/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ToxerPrivate.h"

#include "ToxProfile.h"

#include <QDir>
#include <QFile>
#include <QMap>

#include <tox/toxencryptsave.h>

/** @namespace Toxer
@fn profilesDir
@brief Absolute path to the location of Tox profiles and common data.

@enum Toxer::Global::Context
@brief context for Tox API error strings

@fn Toxer::count
@brief Returns the size a constant static array.
@return the array size
*/

/** @class Toxer::Global
@brief Tox API functions.

@fn Toxer::Global::bcc16
@brief Calculates the 16-bit BCC checksum on a byte array.
@param arr  the byte array
@return the 16-bit BCC checksum

@fn Toxer::Global::address_bcc
@brief Extracts the checksum from a Tox address.
@param tox_addr   the (binary) Tox address
@return The 16-bit BCC checksum.

@fn Toxer::Global::check
@brief Performs a checksum check on a Tox address.
@param tox_addr   the (binary) Tox address
@return true when the address has a valid checksum; false otherwise

@fn Toxer::Global::pk
@brief Returns a friend's public key.
@param[in] tox          the valid Tox instance
@param[in] friendIndex  the friend index
@return the public key as raw data or an empty byte array

The profile's public key can be returned by passing -1 as the friendIndex.
*/

/** @class Toxer::Private
@brief Global private Toxer management object.

@note This is the only static object in ToxerCore which lives as long as main().
*/
class Toxer::Private final {
private:
  using ToxKeys = QMap<QString, Tox_Pass_Key*>;
  ToxKeys keys;

public:
  ToxProfile::Ptr activeProfile;

public:
  Private() = default;
  ~Private() {
    // free remaining Tox_Pass_Keys in purse
    while (keys.size()) {
      freeKey(keys.firstKey());
    }
  }

private:
  // disable copy
  Private(const Private& other) = delete;
  Private(Private&& other) = delete;
  Private& operator=(const Private& other) = delete;
  Private& operator=(Private&& other) = delete;

public:
  void createKey(const QString& tox_key_id,
                 const char* password,
                 const char* cyphertext)
  {
    Q_ASSERT(cyphertext);
    Q_ASSERT(!keys.contains(tox_key_id));

    TOX_ERR_GET_SALT err_salt;
    uint8_t salt[TOX_PASS_SALT_LENGTH];
    tox_get_salt(to_tox(cyphertext), salt, &err_salt);
    Q_ASSERT(err_salt == TOX_ERR_GET_SALT_OK);

    TOX_ERR_KEY_DERIVATION err;
    auto pw_len = strlen(password);
    auto tpk = tox_pass_key_derive_with_salt(to_tox(password), pw_len, salt, &err);
    Q_ASSERT(err == TOX_ERR_KEY_DERIVATION_OK);

    keys.insert(tox_key_id, tpk);
  }

  bool hasKey(const QString& tox_key_id) {
    return keys.contains(tox_key_id);
  }

  void freeKey(const QString& tox_key_id) {
    auto tpk = keys.take(tox_key_id);
    tox_pass_key_free(tpk);
  }

  QByteArray encrypt(const QString& tox_key_id,
                     const char* raw,
                     size_t raw_len)
  {
    auto tpk = keys[tox_key_id]; Q_ASSERT(tpk);
    auto encrypted_len = raw_len + TOX_PASS_ENCRYPTION_EXTRA_LENGTH;
    auto encrypted = new char[encrypted_len];
    TOX_ERR_ENCRYPTION err;
    tox_pass_key_encrypt(tpk, to_tox(raw), raw_len, to_tox(encrypted), &err);
    if (err) {
      qWarning("Encryption failed: %s", err_str(err, Context::Encrypt));
      delete[] encrypted;
      return QByteArray();
    }

    return QByteArray::fromRawData(encrypted, static_cast<int>(encrypted_len));
  }

  QByteArray decrypt(const QString& tox_key_id,
                     const char* cyphertext,
                     size_t len)
  {
    if (len < tox_pass_encryption_extra_length()) {
      qWarning("Encrypted data must have minimum size of %d bytes.",
               tox_pass_encryption_extra_length());
      return QByteArray();
    }

    auto tpk = keys[tox_key_id]; Q_ASSERT(tpk);
    auto decrypted_len = len - TOX_PASS_ENCRYPTION_EXTRA_LENGTH;
    auto decrypted = new char[decrypted_len];
    TOX_ERR_DECRYPTION err;
    tox_pass_key_decrypt(tpk, to_tox(cyphertext), len, to_tox(decrypted), &err);
    if (err) {
      qWarning("Decryption failed: %s", err_str(err, Context::Decrypt));
      Private::freeKey(tox_key_id); // free key on failure
      delete[] decrypted;
      return QByteArray();
    }

    return QByteArray::fromRawData(decrypted, static_cast<int>(decrypted_len));
  }
};

// private global instance
static Toxer::Private toxer_p;

/** @brief return the active Tox profile or nullptr when none is active. */
ToxProfile* Toxer::activeProfile() { return toxer_p.activeProfile.get(); }

/** @brief Activates a Tox profile.
@note A previously active profile will be closed.
*/
void Toxer::activate(const QString& profileName, const QString& password)
{
  if (toxer_p.activeProfile && toxer_p.activeProfile->name() == profileName) {
    return;
  }

  auto profileData = load(profileName, password);
  if (!profileData.isEmpty()) {
    toxer_p.activeProfile.reset(new ToxProfile(profileName, profileData));
    toxer_p.activeProfile->start();
  } else {
    qWarning("Tox profile not activated: Wrong password.");
  }
}

/** @brief Loads a Tox profile.
@param[in] profileName  the profile name
@param[in] password     the profile password
@return the Tox data
*/
QByteArray Toxer::load(const QString& profileName, const QString& password)
{
  QFile f(Toxer::profilesDir() % QString::fromUtf8("/%1.tox").arg(profileName));
  if (!f.exists()) {
    qWarning("No Tox file found at \"%s\"", qUtf8Printable(f.fileName()));
    return {};
  }

  if (f.open(QFile::ReadOnly)) {
    const QByteArray data = f.readAll();
    if (Toxer::isEncrypted(data.constData(), data.length())) {
      return Toxer::decrypt(profileName, data, password);
    } else {
      qWarning("Toxer does not support unencrypted profiles!"
               " Encrypt the profile now or create a new one!");
      return {};
    }
  } else {
    qWarning("Failed to open Tox profile %s.\nError message: %s",
             qUtf8Printable(profileName), qUtf8Printable(f.errorString()));
    return {};
  }
}

/** @brief Maps a Tox error to a readable message.
@param[in] err  the error number
@param[in] ctx  the error context
@return The message.
*/
const char* Toxer::err_str(int err, Context ctx)
{
  switch (ctx) {
  case Context::ChunkSend: switch(static_cast<TOX_ERR_FILE_SEND_CHUNK>(err)) {
    case TOX_ERR_FILE_SEND_CHUNK_OK: return toxErrStrOk;
    case TOX_ERR_FILE_SEND_CHUNK_FRIEND_NOT_CONNECTED: return toxErrStrNoConn;
    case TOX_ERR_FILE_SEND_CHUNK_FRIEND_NOT_FOUND: return toxErrStrFriend;
    case TOX_ERR_FILE_SEND_CHUNK_INVALID_LENGTH: return "invalid chunk length";
    case TOX_ERR_FILE_SEND_CHUNK_NOT_FOUND: return "chunk not found";
    case TOX_ERR_FILE_SEND_CHUNK_NOT_TRANSFERRING: return "chunk not transferring";
    case TOX_ERR_FILE_SEND_CHUNK_NULL: return toxErrStrNull;
    case TOX_ERR_FILE_SEND_CHUNK_SENDQ: return "package queue is full";
    case TOX_ERR_FILE_SEND_CHUNK_WRONG_POSITION: return "invalid chunk position";
    } break;
  case Context::Decrypt: switch(static_cast<TOX_ERR_DECRYPTION>(err)) {
    case TOX_ERR_DECRYPTION_OK:             return toxErrStrOk;
    case TOX_ERR_DECRYPTION_NULL:           return toxErrStrNull;
    case TOX_ERR_DECRYPTION_FAILED:         return "decryption failed";
    case TOX_ERR_DECRYPTION_BAD_FORMAT:     return "decryption format mismatch";
    case TOX_ERR_DECRYPTION_INVALID_LENGTH: return "encrypted length too short";
    case TOX_ERR_DECRYPTION_KEY_DERIVATION_FAILED: return "key derivation failed";
    } break;
  case Context::Encrypt: switch(static_cast<TOX_ERR_ENCRYPTION>(err)) {
    case TOX_ERR_ENCRYPTION_OK:                     return toxErrStrOk;
    case TOX_ERR_ENCRYPTION_NULL:                   return toxErrStrNull;
    case TOX_ERR_ENCRYPTION_FAILED:                 return "encryption failed";
    case TOX_ERR_ENCRYPTION_KEY_DERIVATION_FAILED:  return "key derivation failed";
    } break;
  case Context::FileControl: switch(static_cast<TOX_ERR_FILE_CONTROL>(err)) {
    case TOX_ERR_FILE_CONTROL_OK: return toxErrStrOk;
    case TOX_ERR_FILE_CONTROL_SENDQ: return "packet queue is full";
    case TOX_ERR_FILE_CONTROL_DENIED: return "transfer is paused by the receiver; cannot resume";
    case TOX_ERR_FILE_CONTROL_NOT_FOUND: return "file number not found";
    case TOX_ERR_FILE_CONTROL_ALREADY_PAUSED: return "transfer already paused";
    case TOX_ERR_FILE_CONTROL_NOT_PAUSED: return "transfer is not paused; cannot resume";
    case TOX_ERR_FILE_CONTROL_FRIEND_NOT_CONNECTED: return toxErrStrNoConn;
    case TOX_ERR_FILE_CONTROL_FRIEND_NOT_FOUND: return toxErrStrFriend;
    } break;
  case Context::FileSend: switch(static_cast<TOX_ERR_FILE_SEND>(err)) {
    case TOX_ERR_FILE_SEND_OK: return toxErrStrOk;
    case TOX_ERR_FILE_SEND_FRIEND_NOT_CONNECTED: return toxErrStrNoConn;
    case TOX_ERR_FILE_SEND_FRIEND_NOT_FOUND: return toxErrStrFriend;
    case TOX_ERR_FILE_SEND_NULL: return toxErrStrNull;
    case TOX_ERR_FILE_SEND_NAME_TOO_LONG: return "file name too long";
    case TOX_ERR_FILE_SEND_TOO_MANY: return "exceeded the maximum number of ongoing transfers";
    } break;
  case Context::FileReceive: switch(static_cast<TOX_ERR_FILE_GET>(err)) {
    case TOX_ERR_FILE_GET_OK: return toxErrStrOk;
    case TOX_ERR_FILE_GET_FRIEND_NOT_FOUND: return toxErrStrFriend;
    case TOX_ERR_FILE_GET_NOT_FOUND: return "file not found";
    case TOX_ERR_FILE_GET_NULL: return toxErrStrNull;
    } break;
  case Context::FriendAdd: switch(static_cast<TOX_ERR_FRIEND_ADD>(err)) {
    case TOX_ERR_FRIEND_ADD_OK:             return toxErrStrOk;
    case TOX_ERR_FRIEND_ADD_NULL:           return toxErrStrNull;
    case TOX_ERR_FRIEND_ADD_MALLOC:         return toxErrStrMalloc;
    case TOX_ERR_FRIEND_ADD_TOO_LONG:       return "friend request message too long";
    case TOX_ERR_FRIEND_ADD_NO_MESSAGE:     return "friend request is empty";
    case TOX_ERR_FRIEND_ADD_OWN_KEY:        return "own PK cannot be added as friend";
    case TOX_ERR_FRIEND_ADD_ALREADY_SENT:   return "friend request already sent";
    case TOX_ERR_FRIEND_ADD_BAD_CHECKSUM:   return "wrong checksum on friend PK";
    case TOX_ERR_FRIEND_ADD_SET_NEW_NOSPAM: return "friend exists with different NOSPAM";
    } break;
  case Context::FriendDelete: switch(static_cast<TOX_ERR_FRIEND_DELETE>(err)) {
    case TOX_ERR_FRIEND_DELETE_OK: return toxErrStrOk;
    case TOX_ERR_FRIEND_DELETE_FRIEND_NOT_FOUND: return toxErrStrFriend;
    } break;
  case Context::FriendPK: switch(static_cast<TOX_ERR_FRIEND_GET_PUBLIC_KEY>(err)) {
    case TOX_ERR_FRIEND_GET_PUBLIC_KEY_OK: return toxErrStrOk;
    case TOX_ERR_FRIEND_GET_PUBLIC_KEY_FRIEND_NOT_FOUND: return toxErrStrFriend;
    } break;
  case Context::Salt: switch(static_cast<TOX_ERR_GET_SALT>(err)) {
    case TOX_ERR_GET_SALT_OK:           return toxErrStrOk;
    case TOX_ERR_GET_SALT_NULL:         return toxErrStrNull;
    case TOX_ERR_GET_SALT_BAD_FORMAT:   return toxErrStrFormat;
    } break;
  case Context::New: switch (static_cast<TOX_ERR_NEW>(err)) {
    case TOX_ERR_NEW_OK:                return toxErrStrOk;
    case TOX_ERR_NEW_NULL:              return toxErrStrNull;
    case TOX_ERR_NEW_MALLOC:            return toxErrStrMalloc;
    case TOX_ERR_NEW_LOAD_BAD_FORMAT:   return toxErrStrFormat;
    case TOX_ERR_NEW_LOAD_ENCRYPTED:    return "decryption failed";
    case TOX_ERR_NEW_PORT_ALLOC:        return "out of resources";
    case TOX_ERR_NEW_PROXY_BAD_HOST:    return "invalid proxy hostname";
    case TOX_ERR_NEW_PROXY_BAD_PORT:    return "invalid proxy port";
    case TOX_ERR_NEW_PROXY_BAD_TYPE:    return "proxy type not supported";
    case TOX_ERR_NEW_PROXY_NOT_FOUND:   return "proxy host not found";
    } break;
  case Context::Message: switch(static_cast<TOX_ERR_FRIEND_SEND_MESSAGE>(err)) {
    case TOX_ERR_FRIEND_SEND_MESSAGE_OK:                    return toxErrStrOk;
    case TOX_ERR_FRIEND_SEND_MESSAGE_NULL:                  return toxErrStrNull;
    case TOX_ERR_FRIEND_SEND_MESSAGE_EMPTY:                 return "empty message";
    case TOX_ERR_FRIEND_SEND_MESSAGE_SENDQ:                 return toxErrStrMalloc;
    case TOX_ERR_FRIEND_SEND_MESSAGE_TOO_LONG:              return "message too long";
    case TOX_ERR_FRIEND_SEND_MESSAGE_FRIEND_NOT_FOUND:      return toxErrStrFriend;
    case TOX_ERR_FRIEND_SEND_MESSAGE_FRIEND_NOT_CONNECTED:  return "not connected to friend";
    } break;
  case Context::SetInfo: switch (static_cast<TOX_ERR_SET_INFO>(err)) {
    case TOX_ERR_SET_INFO_OK:       return toxErrStrOk;
    case TOX_ERR_SET_INFO_NULL:     return toxErrStrNull;
    case TOX_ERR_SET_INFO_TOO_LONG: return "information too long";
    } break;
  case Context::Query: switch (static_cast<TOX_ERR_FRIEND_QUERY>(err)) {
    case TOX_ERR_FRIEND_QUERY_OK: return toxErrStrOk;
    case TOX_ERR_FRIEND_QUERY_NULL: return toxErrStrNull;
    case TOX_ERR_FRIEND_QUERY_FRIEND_NOT_FOUND: return toxErrStrFriend;
    }
  }

  return "unknown error";
}

/** @brief Check if data is encrypted.
@param data     the data array to check
@param len      only used to perform a data length check
@return true when data is encrypted; false otherwise
@note Currently Tox can only check null-terminated string data.

The data is considered unencrypted when data equals nullptr or data length is
smaller than TOX_PASS_ENCRYPTION_EXTRA_LENGTH.
Otherwise the Tox check is performed.
*/
bool Toxer::isEncrypted(const char* data, int len) {
  return data && len >= TOX_PASS_ENCRYPTION_EXTRA_LENGTH
      ? tox_is_data_encrypted(to_tox(data))
      : false;
}

/** @brief Decrypts data using a registered key or a password.
@param tox_key_id    the key identifier
@param data          the encrypted data
@param pass          the password (optional)
@return the plaintext array
*/
QByteArray Toxer::decrypt(const QString& tox_key_id,
                                 const QByteArray& data,
                                 const QString& pass)
{
  if (Q_UNLIKELY(!toxer_p.hasKey(tox_key_id))) {
    Q_ASSERT(!pass.isEmpty());
    auto c_pass = pass.toUtf8();
    toxer_p.createKey(tox_key_id, c_pass.constData(), data.constData());
  }
  return toxer_p.decrypt(tox_key_id, data.constData(),
                       static_cast<size_t>(data.length()));
}

/** @brief Encrypts data using a Tox key or password.
@param tox_key_id   the identifier to the Tox key
@param rawData      the unencrypted raw data
@param len          the length of unencrypted raw data
@param password     the password
@return the encrypted data as QByteArray
*/
QByteArray Toxer::encrypt(const QString& tox_key_id, const char* data,
                                 size_t len, const QString& password)
{
  if (Q_LIKELY(toxer_p.hasKey(tox_key_id))) {
    return toxer_p.encrypt(tox_key_id, data, len);
  }

  Q_ASSERT(!password.isEmpty());
  auto pw = password.toUtf8();
  auto encrypted_len = len + TOX_PASS_ENCRYPTION_EXTRA_LENGTH;
  auto out = new char[encrypted_len];
  TOX_ERR_ENCRYPTION err;
  tox_pass_encrypt(to_tox(data),
                   static_cast<size_t>(len),
                   to_tox(pw.constData()),
                   static_cast<size_t>(pw.length()),
                   to_tox(out),
                   &err);
  if (err) {
    qWarning("Encryption failed: %s", err_str(err, Context::Encrypt));
    delete[] out;
    return QByteArray();
  }
  toxer_p.createKey(tox_key_id, pw.constData(), out);
  return QByteArray::fromRawData(out, static_cast<int>(encrypted_len));
}

/** @brief Free's a key in the purse.
@param tox_key_id   the key identifier
*/
void Toxer::removeToxKey(const QString &tox_key_id) {
  toxer_p.freeKey(tox_key_id);
}

/** @brief conversion from Tox to Qt */
ToxTypes::Proxy Toxer::fromTox(TOX_PROXY_TYPE enumeration)
{
  switch (enumeration) {
  case TOX_PROXY_TYPE_NONE: return ToxTypes::Proxy::None;
  case TOX_PROXY_TYPE_HTTP: return ToxTypes::Proxy::HTTP;
  case TOX_PROXY_TYPE_SOCKS5: return ToxTypes::Proxy::SOCKS5;
  }

  Q_ASSERT(false);
  return ToxTypes::Proxy::None;
}

/** @brief conversion from Qt to Tox */
TOX_PROXY_TYPE Toxer::toTox(ToxTypes::Proxy enumeration)
{
  switch (enumeration) {
  case ToxTypes::Proxy::None: return TOX_PROXY_TYPE_NONE;
  case ToxTypes::Proxy::HTTP: return TOX_PROXY_TYPE_HTTP;
  case ToxTypes::Proxy::SOCKS5: return TOX_PROXY_TYPE_SOCKS5;
  }

  Q_ASSERT(false);
  return TOX_PROXY_TYPE_NONE;
}

/** @brief conversion from Tox to Qt */
ToxTypes::Transport Toxer::fromTox(TOX_FILE_CONTROL enumeration)
{
  switch(enumeration) {
  case TOX_FILE_CONTROL_RESUME: return ToxTypes::Transport::Resume;
  case TOX_FILE_CONTROL_PAUSE: return ToxTypes::Transport::Pause;
  case TOX_FILE_CONTROL_CANCEL: return  ToxTypes::Transport::Cancel;
  }

  Q_ASSERT(false);
  return ToxTypes::Transport::Cancel;
}

/** @brief conversion from Qt to Tox */
TOX_FILE_CONTROL Toxer::toTox(ToxTypes::Transport enumeration) {
  switch(enumeration) {
  case ToxTypes::Transport::Resume: return TOX_FILE_CONTROL_RESUME;
  case ToxTypes::Transport::Pause: return TOX_FILE_CONTROL_PAUSE;
  case ToxTypes::Transport::Cancel: return  TOX_FILE_CONTROL_CANCEL;
  }

  Q_ASSERT(false);
  return TOX_FILE_CONTROL_CANCEL;
}

/** @brief conversion from Qt to Tox */
ToxTypes::UserStatus Toxer::fromTox(TOX_USER_STATUS enumeration)
{
  switch (enumeration) {
  case TOX_USER_STATUS_NONE: return ToxTypes::UserStatus::Ready;
  case TOX_USER_STATUS_AWAY: return ToxTypes::UserStatus::Away;
  case TOX_USER_STATUS_BUSY: return ToxTypes::UserStatus::Busy;
  }

  Q_ASSERT(false);
  return ToxTypes::UserStatus::Unknown;
}

/** @brief conversion from Qt to Tox */
TOX_USER_STATUS Toxer::toTox(ToxTypes::UserStatus enumeration)
{
  switch (enumeration) {
  case ToxTypes::UserStatus::Ready: return TOX_USER_STATUS_NONE;
  case ToxTypes::UserStatus::Busy: return TOX_USER_STATUS_BUSY;
  case ToxTypes::UserStatus::Away:
  case ToxTypes::UserStatus::Unknown: return TOX_USER_STATUS_AWAY;
  }

  Q_ASSERT(false);
  return TOX_USER_STATUS_AWAY;
}
